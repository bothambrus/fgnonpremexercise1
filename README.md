# fgNonPremExercise1

This repository contains a tutorial of creating a tabulated-chemistry lookup table based on non-premixed counterflow flamelets.

Follow the steps in the [wiki](https://gitlab.com/bothambrus/fgnonpremexercise1/-/wikis/home).
