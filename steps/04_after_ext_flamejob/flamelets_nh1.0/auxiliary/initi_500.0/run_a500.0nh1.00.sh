#!/bin/bash
#SBATCH -D /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/auxiliary/initi_500.0/
#SBATCH --job-name=a500.0nh1.00
#SBATCH --output=fg_a500.0nh1.00_%j.out
#SBATCH --error=fg_a500.0nh1.00_%j.err
#SBATCH --ntasks=1
#SBATCH --time=02:00:00
#SBATCH --qos=debug
module -q purge
module load intel/2017.4
module load impi/2017.4
module load mkl/2017.4

/gpfs/projects/bsc21/bsc21304/FlameGen/chem1d/bin/chem1d

mkdir -p /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/auxiliary/initi_500.0/../../stable
echo "Copy and modify to steady state: /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/auxiliary/initi_500.0/yiend.dat > /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/auxiliary/initi_500.0/../../stable/yiend_500.0.dat"
awk '{sub("Time Dependent solution","Stationary solution")}1' /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/auxiliary/initi_500.0/yiend.dat > /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/auxiliary/initi_500.0/../../stable/yiend_500.0.dat
awk '{sub("Time Dependent solution","Stationary solution")}1' /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/auxiliary/initi_500.0/siend.dat > /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/auxiliary/initi_500.0/../../stable/siend_500.0.dat
echo "Done copying"
