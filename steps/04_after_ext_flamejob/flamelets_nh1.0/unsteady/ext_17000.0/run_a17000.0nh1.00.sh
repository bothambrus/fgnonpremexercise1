#!/bin/bash
#SBATCH -D /gpfs/scratch/bsc21/bsc21304/fgnonpremexercise1/exercise/flamelets_nh1.0/unsteady/ext_17000.0/
#SBATCH --job-name=a17000.0nh1.00
#SBATCH --output=fg_a17000.0nh1.00_%j.out
#SBATCH --error=fg_a17000.0nh1.00_%j.err
#SBATCH --ntasks=1
#SBATCH --time=02:00:00
#SBATCH --qos=debug
module -q purge
module load intel/2017.4
module load impi/2017.4
module load mkl/2017.4

/gpfs/projects/bsc21/bsc21304/FlameGen/chem1d/bin/chem1d

