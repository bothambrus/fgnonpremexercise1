#!/bin/bash
#SBATCH --job-name=calcFlam
#SBATCH --output=mpi_%j.out
#SBATCH --error=mpi_%j.err
#SBATCH --ntasks=5
#SBATCH --cpus-per-task=1
#SBATCH --time=02:00:00
#SBATCH --qos=debug 
srun /gpfs/projects/bsc21/bsc21304/flamegen/fg flamelet 
