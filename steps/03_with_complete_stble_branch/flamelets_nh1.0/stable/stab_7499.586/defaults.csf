###############################################
#                                             #
#  CHEM1D 3.22                                #
#         -> Casefile written by FlameGen     #
#                                             #
###############################################

#
# solver
#
[SOLVER_CONVERGENCE]                    => Typical value 1.0e-10
1e-11
[SOLVER_MAXIMUMITERATIONS]              => Typical value 100000
1500
[SOLVER_DIFFERENTIALSCHEME]             => Options: UPWIND, CENTRAL, EXPONENTIAL
EXPONENTIAL
[SOLVER_TIMEINTERGRATION]               => Options: BDF1, BDF2
BDF2

#
# model
#
[MODEL_FLAMETYPE]                       => Options: FREE, COUNTERFLOW, COUNTERFLOWCYL, BURNERSTABILIZED, ORIGIN, PLATEHEATING
COUNTERFLOW
[MODEL_SIMULATIONTYPE]                  => Options: TIMEDEPENDENT, QUASITIMEDEPENDENT, STATIONARY
STATIONARY
[MODEL_TRANSPORT]                       => Options: LEWIS, MIXTUREAVERAGED, COMPLEX
LEWIS
[MODEL_THERMALDIFFUSION]                => Options: ON/OFF
OFF
[MODEL_CHEMISTRY]                       => Options: DETAILED, FGM
DETAILED
[MODEL_RADIATION]                       => Options: NONE, PLANCK
NONE

#
# timestepper
#
[TIMESTEPPER_INITIALTIMESTEP]           => Typical value 1.0e-6
1e-09
[TIMESTEPPER_MINIMUMTIMESTEP]           => Typical value 1.0e-10
1e-13
[TIMESTEPPER_MAXIMUMTIMESTEP]           => Typical value 1.0e-4
0.1
[TIMESTEPPER_MAXSIMULATIONTIME]         => Typical value 1.0
1.0
[TIMESTEPPER_OUTPUTSTEP]                => Typical value 100
20

#
# boundary
#
[BOUNDARY_MASSFLOW]                     => Only for burnerstabilized flames
0.0
[BOUNDARY_INLETVELOCITY]                => Only for burnerstabilized flames
1.0
[BOUNDARY_EQUIVALENCERATIO]             => left and right equivalence ratio
1.0 0.0 
[BOUNDARY_MIXTUREFRACTION]              => left and right mixture fraction
1.0 0.0 
[BOUNDARY_INLETTEMPERATURE]             => fuel and oxidizer temperature
320.0000002190059 453.0 
[BOUNDARY_INLETPRESSURE]                => pressure
101325.0
[BOUNDARY_MIXTURECALCULATION]           => Options: EQUIVALENCERATIO, MIXTUREFRACTION, MOLE, MASS, CONC
MIXTUREFRACTION

#
# gasmixture
#
[GASMIXTURE_MIXTURECOMPOSITION]         => Unit : Defined in BOUNDARY_MIXTURECALCULATION
N2 1.0 1.0
[ENDOF_MIXTURECOMPOSITION]
[GASMIXTURE_FUELCOMPOSITION]            => Unit : MOLE fractions, only together with EQUIVALENCERATIO or MIXTUREFRACTION
H2 1.0
[ENDOF_FUELCOMPOSITION]
[GASMIXTURE_OXIDIZERCOMPOSITION]        => Unit : MOLE fractions, only together with EQUIVALENCERATIO or MIXTUREFRACTION
N2 0.79
O2 0.21
[ENDOF_OXIDIZERCOMPOSITION]
[GASMIXTURE_BATHGAS]                    => Info : Abundant species, often N2
N2
[GASMIXTURE_FUCONV]                     => Info : Not supported
0.0 0.0 0.0 

#
# grid
#
[GRID_NUMBEROFPOINTS]                   => Typical value: 100
200
[GRID_WEIGHT]                           => Default = 1, lower = less important, higher = more important during regridding
HeatRel 2.0
Massflow 0.4
Temp 2.0
[ENDOF_WEIGHT]
[GRID_SETTOZERO]                        => Options: ON, OFF
ON
[GRID_REGRID]                           => Options: ON, OFF
ON
[GRID_UNIFORMITYMESH]                   => Typical value: 0.15
0.15
[GRID_MESHRATIO]                        => Typical value: 2.0
2.0
[GRID_LEFTBOUNDARY]                     => Typical value: -0.5
-0.13230113336260726
[GRID_RIGHTBOUNDARY]                    => Typical value : 2.0
0.19845170004389603
[GRID_FIXEDPOINT]                       => Variable name and value
Temp -100 
[GRID_ADAPTATIONINTERVAL]               => Typical value : 1  0
2 0 
[GRID_INTERPOLATION]                    => Options : LINEAR, SPLINE
LINEAR

#
# preprocessing
#
[PREPROCESSING_STARTSOLUTIONFILE]       => Typical value: yistart.dat
yistart.dat
[PREPROCESSING_REACTIONMECHANISMFILE]   => Typical value: *.chm
gri30.chm

#
# postprocessing
#
[POSTPROCESSING_OUTPUTFILES]            => Supply a filename to the default ouput files
CONC            ciend.dat            OFF
EQUIL           yiequil.dat          OFF
LAMBDACP        lambdacp.dat         OFF
LEWIS           lewis.dat            OFF
MASS            yiend_7499.586.dat   ON 
MOLE            xiend.dat            OFF
REACTIONRATES   rates.dat            OFF
RESIDUAL        residu.dat           OFF
SOURCE          siend_7499.586.dat   ON 
[ENDOF_OUTPUTFILES]
[POSTPROCESSING_USEROUTPUT]             => Options: Defined by user in postprocessing.f
FLUENT          flamelet.dat         OFF
[ENDOF_USEROUTPUT]
[POSTPROCESSING_SENSITIVITY]            => Options: NONE, REACTIONS, ALL
NONE

#
# radiation
#
[RADIATION_FACTOR]                      => Info: Artificial scaling factor for Planck Mean Absorbtion Coefficient (calculated internally for H2O and CO2)
0.0

#
# flameacoustics
#
[FLAMEACOUSTICS_OSCILLATION]            => Options: NONE, BLOCK, FTHRE
NONE
[FLAMEACOUSTICS_FREQUENCY]              => Typical value: 1.953125  Hz
100.0
[FLAMEACOUSTICS_AMPLITUDE]              => Typical value: 1.7e-4  cm/s
1.0
[FLAMEACOUSTICS_TIMEINTERVAL]           => Typical values: 2.5e-4 ... 0.51225
0.0 0.0 

#
# lewis
#
[LEWIS_TRANSPORTFILE]                   => Typical value  : lewis.dat
lewis.dat
[LEWIS_DIFFUSIONCORRECTION]             => Options: Speciesname (N2)
N2
[LEWIS_LAMBDACP]                        => Typical values : 2.58e-4 0.690
0.000258 0.69 
[LEWIS_ETACP]                           => Typical values : 1.67e-4 0.51
0.000167 0.51 

#
# mixtureaveraged
#
[MIXTUREAVERAGED_DIFFUSIONCORRECTION]   => Options : ALL, or speciesname like N2
N2

#
# solid
#
[SOLID_VOLUMETRICPOROSITY]              => Unit : cm^3/cm^3
0.8
[SOLID_AREALPOROSITY]                   => Unit : cm^2/cm^2
0.8
[SOLID_TORTUOSITY]                      => Note : Of the gas!
0.96
[SOLID_LCERAMIC]                        => Unit : cm
1.0
[SOLID_LCOATING]                        => Unit : cm
0.0
[SOLID_SPECIFICSURFACE]                 => Unit : cm^2/cm^3
27.0
[SOLID_ALPHA]                           => Unit : J/(sKcm^2)   alpha=A0 + A1*T
0.1 0.0 
[SOLID_CONDUCTIVITY]                    => Unit : J/(scmK)     lambda=L0 + L1*T
0.0216 0.0 
[SOLID_HEATCAPICITY]                    => Unit : J/(gK)       cp=C0 + C1*T
1.3 0.0 
[SOLID_DENSITY]                         => Unit : g/cm^3
3.0
[SOLID_TSURROUNDINGS]                   => Unit : K
298.0
[SOLID_EXTINCTION]                      => Unit : 1/cm
15.0
[SOLID_ALBEDO]                          => Unit : -
0.0
[SOLID_EMISSIVITY]                      => Unit : -
0.85

#
# strainandcurvature
#
[STRAINANDCURVATURE_FLAMEGEOMETRY]      => Options : FLAT, CYLINDRICAL, SPHERICAL
FLAT
[STRAINANDCURVATURE_CURVATURE]          => Unit : 1/cm   (1/Radius)
0.0
[STRAINANDCURVATURE_STRAIN]             => Unit : 1/s
7499.586
[STRAINANDCURVATURE_STRDOMLEFT]         => Unit : cm
1.0
[STRAINANDCURVATURE_STRDOMRIGHT]        => Unit : cm
1.0
[STRAINANDCURVATURE_FILE]               => File name (stretch.dat)


#
# heat
#
[HEAT_MODEL]                            => Options : NONE, FOURIER
NONE
[HEAT_BOUNDARY_LOCATION]                => Unit : cm
0.5 2.0 
[HEAT_BOUNDARY_TEMP]                    => Unit : K
350
[HEAT_ALPHA]                            => Unit : W/(cm^3 K)
0.05

#
# efield
#
[EFIELD_MODEL]                          
NONE
[EFIELD_BOUNDARY]                       
0.0 0.0 
[EFIELD_BOUNDARY_LOCATION]              
-0.1 2.0 
[EFIELD_CORRECTION]                     
Em

#
# additional
#
[ADDITIONAL_DEBUG]                      => Options : DEBUG, STANDARD, WARNING, ERROR
STANDARD
[ADDITIONAL_USERCOMMENT]                => Info line which will be written in the output files

